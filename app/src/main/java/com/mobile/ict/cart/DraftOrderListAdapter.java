package com.mobile.ict.cart;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


public class DraftOrderListAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<String[]> objects;
    HashMap<String,Double> hashMap = new HashMap<>();
    String stockEnabledStatus;

    DraftOrderListAdapter(Context context, ArrayList<String[]> productList,HashMap<String,Double> hashMap,String stockEnabledStatus) {
        ctx = context;
        objects = productList;
        this.stockEnabledStatus=stockEnabledStatus;
        this.hashMap=hashMap;

        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public String[] getItem(int position) {

        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.activity_draft_order_list_item, parent, false);
        }
        String items[] = getItem(position);

        if(stockEnabledStatus.equals("true"))
        {
            ((TextView) view.findViewById(R.id.stockstatus)).setVisibility(View.VISIBLE);
            HashMap<String, Double> sortList = new HashMap<String, Double>(hashMap);

            for(HashMap.Entry<String, Double> entry : sortList.entrySet())
            {
            if(entry.getKey().equals(items[1]))
            {
                if(entry.getValue()>=Double.parseDouble(items[4]))
                {
                    ((TextView) view.findViewById(R.id.stockstatus)).setText("STOCK AVAILABLE");
                    ((TextView) view.findViewById(R.id.stockstatus)).setTextColor(Color.parseColor("#42A462"));
                }
                else if(entry.getValue()<Double.parseDouble(items[4]))
                {
                    ((TextView) view.findViewById(R.id.stockstatus)).setText("INSUFFICIENT STOCK");
                    ((TextView) view.findViewById(R.id.stockstatus)).setTextColor(Color.BLUE);
                }
                else
                {
                    ((TextView) view.findViewById(R.id.stockstatus)).setText("OUT OF STOCK");
                    ((TextView) view.findViewById(R.id.stockstatus)).setTextColor(Color.RED);
                }

            }
            }
        }
        else
            ((TextView) view.findViewById(R.id.stockstatus)).setVisibility(View.GONE);




        ((TextView) view.findViewById(R.id.draft_order_quantityName)).setText(items[1]);
        ((TextView) view.findViewById(R.id.draft_order_quantity)).setText(items[4]);
        ((TextView) view.findViewById(R.id.draft_order_quantityBasePrice)).setText('\u20B9'+items[2]);
        ((TextView) view.findViewById(R.id.draft_order_quantityTotalPrice)).setText(items[3]);



        return view;
    }


}


package com.mobile.ict.cart;

/**
 * Created by root on 28/3/16.
 */
public interface InfoTaskListener {
    public void onInfo(String imageUrl,String audioUrl,String productName);
}

package com.mobile.ict.cart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

public class DraftToOrder extends Activity implements OnClickListener{

    Button b1 ;
    String memberNumber,draftid,grandtotal,orderid;
    ArrayList<ItemRow> items;
    DraftOrderAdapter draftOrderAdapter;
    ArrayList<DraftOrder> draftOrder;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    TextView grandTotal;
    JSONObject order;
    Intent n = getIntent();
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        setContentView(R.layout.draft);
        Bundle b = getIntent().getExtras();
        String[] resultArr = b.getStringArray("selectedItems");
        String stockEnabledStatus = b.getString("stockEnabledStatus");
        HashMap<String,Double> entryset = (HashMap) getIntent().getSerializableExtra("stockquantitymap");
        draftid = b.getString("draftId");
        grandtotal=b.getString("totalBill");

        grandTotal = (TextView)findViewById(R.id.confirm_draft_order_total_bill);
        grandTotal.setText(grandtotal);

        int lengthOfSelectedItem = resultArr.length;
        items = new ArrayList<ItemRow>();
        draftOrder = new ArrayList<DraftOrder>();

        if(lengthOfSelectedItem<0){
            Toast.makeText(DraftToOrder.this,R.string.label_toast_No_order_is_here, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(),DashboardActivity.class);
            startActivity(intent);
        }
        else
        {
            if(stockEnabledStatus.equals("false"))
            {
                for(int i =0 ; i<lengthOfSelectedItem ; i ++){

                    int aa = resultArr[i].indexOf("\n");
                    String name = resultArr[i].substring(5,aa);
                    int aa1 =resultArr[i].indexOf("Quantity:");
                    int aa2 = resultArr[i].indexOf("Price:");
                    int aa3 = resultArr[i].indexOf("Total:");
                    String quantity= resultArr[i].substring(aa1+9 , aa2);
                    String price = resultArr[i].substring(aa2+6, aa3);
                    String total = resultArr[i].substring(aa3+6);
                    ItemRow item = new ItemRow(name , Double.parseDouble(quantity), Double.parseDouble(price),Double.parseDouble(total));
                    items.add(item);
                    draftOrder.add(new DraftOrder(name,quantity,price,total));
                }
            }
            else
            {
                for(int i =0 ; i<lengthOfSelectedItem ; i ++){

                    int aa = resultArr[i].indexOf("\n");
                    String name = resultArr[i].substring(5, aa);
                    int aa1 =resultArr[i].indexOf("Quantity:");
                    int aa2 = resultArr[i].indexOf("Price:");
                    int aa3 = resultArr[i].indexOf("Total:");
                    String quantity= resultArr[i].substring(aa1+9 , aa2);
                    String price = resultArr[i].substring(aa2 + 6, aa3);
                    String total = resultArr[i].substring(aa3+6);

                    for(HashMap.Entry<String, Double> entry : entryset.entrySet())
                    {
                        if(entry.getKey().equals(name))
                        {
                            if(entry.getValue()>=Double.parseDouble(quantity))
                            {
                                ItemRow item = new ItemRow(name , Double.parseDouble(quantity), Double.parseDouble(price),Double.parseDouble(total));
                                items.add(item);
                                draftOrder.add(new DraftOrder(name,quantity,price,total));
                            }



                        }
                    }




                }
            }
        }

        ListView lv = (ListView) findViewById(R.id.listView1);
        draftOrderAdapter = new DraftOrderAdapter(this,draftOrder);
        lv.setAdapter(draftOrderAdapter);
        ((TextView) findViewById(R.id.tvdraftHead)).setText(getString(R.string.label_selected_items));
        ((Button) findViewById(R.id.button_delete)).setVisibility(View.INVISIBLE);
        ((Button) findViewById(R.id.button_order)).setVisibility(View.INVISIBLE);
        ((Button) findViewById(R.id.button_confirm)).setVisibility(View.VISIBLE);
        ((Space) findViewById(R.id.draft_space)).setVisibility(View.GONE);
        b1 = (Button) findViewById(R.id.button_confirm);
        b1.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if(v== b1){

            try {
                order = new JSONObject();
                order.put("orgabbr",sharedPref.getString("orgAbbr",""));
                order.put("groupname", "Parent Group");
                JSONArray products = new JSONArray();
                JSONObject object;
                int i=1;
                for(ItemRow item : items)
                {
                    object  = new JSONObject();
                    object.put("name",item.getName());
                    object.put("quantity",item.getQuantity().toString());
                    products.put(object);
                    i++;
                }
                order.put("orderItems",products);

            } catch (Exception e) {
                e.printStackTrace();
            }

            new WriteToServer(DraftToOrder.this).execute(order);

        }
    }
    private SQLiteDatabase getReadableDatabase() {
        // TODO Auto-generated method stub
        return null;
    }



    public class WriteToServer extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;
        String val;

        public WriteToServer(Context context){
            this.context=context;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           // lockScreenOrientation();
            pd = new ProgressDialog(DraftToOrder.this);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();

        }

        protected String doInBackground(JSONObject... params) {
            Integer orderId=-1;

            String serverUrl="http://ruralict.cse.iitb.ac.in/RuralIvrs/api/orders/add";
            JsonParser jParser = new JsonParser();
            String response = jParser.getJSONFromUrl(serverUrl,params[0],"POST",true,sharedPref.getString("emailid",null),sharedPref.getString("password",null));

            return response;

        }

        protected void onPostExecute(String response1) {

            pd.dismiss();

            if(response1.equals("exception"))
            {
                Toast.makeText(context,R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later, Toast.LENGTH_SHORT).show();
            }
            else {
                JSONObject jsonObj = null;
                try {

                    jsonObj = new JSONObject(response1);


                      val=jsonObj.getString("status");

                    if(val.equals("Success"))
                    {
                        orderid=jsonObj.getString("orderId");
                        Intent i = new Intent(DraftToOrder.this,OrderSubmitActivity.class);
                        i.putExtra("orderid",orderid);
                        startActivity(i);
                        finish();
                        }

                    else if(val.equals("Failure"))
                    {

                            String error[] = {jsonObj.getString("error")};

                            Toast.makeText(context,getResources().getString(R.string.label_toast_sorry_we_are_unable_to_process_order_items)+error[0] + getResources().getString(R.string.label_toast_out_of_stock), Toast.LENGTH_SHORT).show();


                    }
                    else
                    {
                        Toast.makeText(context,R.string.label_toast_Sorry_we_were_unable_to_process_your_request_Please_try_again_later, Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }





            }


          //  unlockScreenOrientation();


        }



        private void lockScreenOrientation() {
            int currentOrientation = getResources().getConfiguration().orientation;
            if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }

        private void unlockScreenOrientation() {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }

    }


}


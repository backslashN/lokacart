package com.mobile.ict.cart;

import org.json.JSONException;
import org.json.JSONObject;


public class OrderRow {

    private String name;
    private String imageUrl;

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String audioUrl;
    private double unitPrice;
    private double total;
    private double quantity;
    private String stockEnabledStatus;
    private double stockQuantity;

    public String getStockEnabledStatus() {
        return stockEnabledStatus;
    }

    public void setStockEnabledStatus(String stockEnabledStatus) {
        this.stockEnabledStatus = stockEnabledStatus;
    }

    public double getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(double stockQuantity) {
        this.stockQuantity = stockQuantity;
    }



    public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public OrderRow(String name, double unitPrice, double total,double stockQuantity,String stockEnabledStatus,String imageUrl,String audioUrl) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.total = total;
        this.quantity = total/unitPrice;
        this.stockQuantity=stockQuantity;
        this.stockEnabledStatus=stockEnabledStatus;
        this.imageUrl=imageUrl;
        this.audioUrl=audioUrl;
    }

    public OrderRow(String name, double unitPrice, double total) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.total = total;
        this.quantity = total/unitPrice;

    }
    
    public OrderRow(JSONObject orderItem) throws JSONException {
        this.name = orderItem.getString("name");
        this.unitPrice = Double.parseDouble(orderItem.getString("rate"));
        this.quantity = Double.parseDouble(orderItem.getString("quantiity"));
        this.total = this.quantity * this.unitPrice;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}

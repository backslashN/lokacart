package com.mobile.ict.cart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class OrderSuccessActivity extends Activity implements View.OnClickListener{

    Button b1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_success);

        b1 = (Button) findViewById(R.id.button_order_success_done);
        b1.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order, menu);
        MenuItem item_help = menu.findItem(R.id.action_help);
        MenuItem item_logout = menu.findItem(R.id.action_logout);
        MenuItem item_home = menu.findItem(R.id.action_home);
        MenuItem item_refresh = menu.findItem(R.id.action_refresh);
        MenuItem item_editNumber = menu.findItem(R.id.action_edit_number);

        item_editNumber.setVisible(false);
        item_editNumber.setEnabled(false);
        item_refresh.setVisible(false);
        item_refresh.setEnabled(false);
        item_home.setVisible(false);
        item_home.setEnabled(false);

        item_help.setVisible(false);
        item_logout.setVisible(false);
        item_help.setEnabled(false);
        item_logout.setEnabled(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_home) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if(view==b1){
            Intent i = new Intent(this,DashboardActivity.class);
            startActivity(i);
            finishOrderSuccessActivity();
        }
    }
    public void onDestroy() {

        super.onDestroy();

        finish();
    }
    public void finishOrderSuccessActivity() {
        OrderSuccessActivity.this.finish();
    }

}

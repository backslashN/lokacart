package com.mobile.ict.cart;

import android.content.Context;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ModifyOrderRowAdapter extends ArrayAdapter<OrderRow>{

    private static final String TAG = "OrderRowArrayAdapter";
    private List<OrderRow> orderItemList = new ArrayList<OrderRow>();
    OrderRowViewHolder viewHolder;
    HashMap<String,Double> qmap = new HashMap<String, Double>();
    Context context;
    Bundle b;
    HashMap<String,Double> hashMap = new HashMap<>();
    String stockEnabledStatus;
    HashMap<String,Double> quantities = new HashMap<String, Double>();
    boolean flag=false;


    public ModifyOrderRowAdapter(Context context, int resource, List<OrderRow> orderItemList, Bundle bundle,HashMap<String,Double> hashMap,String stockEnabledStatus) {
        super(context, resource, orderItemList);
        this.orderItemList = orderItemList;
        this.context = context;
        this.stockEnabledStatus=stockEnabledStatus;
        this.hashMap=hashMap;
        this.b = bundle;
    }

    static class OrderRowViewHolder {
        TextView name;
        TextView unitPrice;
        EditText quantity;
        TextView total;
        TextView stockStatus;
    }

    @Override
    public void add(OrderRow object) {
        orderItemList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.orderItemList.size();
    }

    @Override
    public OrderRow getItem(int index) {
        return this.orderItemList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.order_row, parent, false);
            viewHolder = new OrderRowViewHolder();
            viewHolder.name = (TextView) row.findViewById(R.id.new_order_item_name);
            viewHolder.unitPrice = (TextView) row.findViewById(R.id.new_order_item_unit_rate);
            viewHolder.quantity = (EditText) row.findViewById(R.id.new_order_item_quantity);
            viewHolder.total = (TextView) row.findViewById(R.id.new_order_item_bill);
            viewHolder.stockStatus = (TextView) row.findViewById(R.id.stockstatus);
            row.setTag(viewHolder);
        } else {
            viewHolder = (OrderRowViewHolder)row.getTag();
        }
        OrderRow item = getItem(position);
        viewHolder.name.setText(item.getName());
        viewHolder.unitPrice.setText('\u20B9'+item.getUnitPrice().toString());

        HashMap<String, Double> sortList = new HashMap<String, Double>(hashMap);
        double quantity;
        if(qmap.containsKey(item.getName())){
            quantity = qmap.get(item.getName());
        }
        else if(b.containsKey(item.getName())){
            quantity = b.getDouble(item.getName(),0.0);
        }
        else{
            quantity = 0;
            qmap.put(item.getName(),0.0);
        }

        viewHolder.quantity.setText(quantity + "");
        item.setTotal(Double.parseDouble(String.format("%.2f", quantity * item.getUnitPrice())));
        viewHolder.total.setText(item.getTotal().toString());

        if(stockEnabledStatus.equals("true")) {
            viewHolder.stockStatus.setVisibility(View.VISIBLE);




            for(HashMap.Entry<String, Double> entry : sortList.entrySet())
            {
                if(entry.getKey().equals(item.getName()))
                {
                    if(entry.getValue()!=0.0)
                    {
                        ((TextView)row.findViewById(R.id.stockstatus)).setText("STOCK AVAILABLE");
                        viewHolder.quantity.setEnabled(true);
                        viewHolder.stockStatus.setTextColor(Color.parseColor("#42A462"));
                    }
                    else
                    {
                        ((TextView)row.findViewById(R.id.stockstatus)).setText("OUT OF STOCK");
                        viewHolder.quantity.setEnabled(false);
                        viewHolder.stockStatus.setTextColor(Color.RED);
                    }

                }
            }


        }
        else
        {

            viewHolder.stockStatus.setVisibility(View.GONE);
            viewHolder.quantity.setEnabled(true);
        }


        final EditText quantity_field = (EditText) row.findViewById(R.id.new_order_item_quantity);
        final TextView total_field = (TextView) row.findViewById(R.id.new_order_item_bill);
        final TextView rate_field = (TextView) row.findViewById(R.id.new_order_item_unit_rate);
        final TextView item_name = (TextView) row.findViewById(R.id.new_order_item_name);

        final String unitRate = rate_field.getText().toString().substring(1);


        final String stockEnbldStatus = stockEnabledStatus;

        quantity_field.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {


                if (!quantity_field.getText().toString().equals("")) {



                    try {
                        Double.parseDouble(quantity_field.getText().toString());
                        String str = String.format("%.2f", (Double.parseDouble(unitRate) * Double.parseDouble(quantity_field.getText().toString())));
                        total_field.setText(str + " INR");
                        b.putDouble(item_name.getText().toString(), Double.parseDouble(quantity_field.getText().toString()));

                    } catch (NumberFormatException e) {
                        Toast.makeText(context, R.string.label_toast_Please_enter_valid_quantity, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    total_field.setText((Double.parseDouble(unitRate) * 0.0) + " INR");
                    b.putDouble(item_name.getText().toString(), 0.0);

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }
        });

        quantity_field.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasFocus) {
           if(hasFocus) {
               if (stockEnbldStatus.equals("true")) {

                   if (quantity_field.isEnabled())
                   {
                       try {
                           Double.parseDouble(quantity_field.getText().toString());
                           HashMap<String, Double> sortList = new HashMap<String, Double>(hashMap);
                           for (HashMap.Entry<String, Double> entry : sortList.entrySet()) {
                               if (entry.getKey().equals(item_name.getText().toString())) {
                                   if (entry.getValue() < Double.parseDouble(quantity_field.getText().toString()))

                                   {
                                       Toast.makeText(context, R.string.label_insufficeint_stock, Toast.LENGTH_SHORT).show();
                                       break;
                                   }
                               }
                           }
                       }
                       catch (NumberFormatException e) {
                       }



                   }

               }
           }


            }
        });



        return row;
    }


}
package com.mobile.ict.cart;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.mobile.ict.cart.ProductCategory;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 9/10/15.
 */
public class OrderRowAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private ArrayList<ProductCategory> mGroups;
    private LayoutInflater mInflater;
    OrderRowViewHolder viewHolder;
    InfoTaskListener callback;
    Bundle b;
    Bundle stockqnty = new Bundle();

    public OrderRowAdapter(Context context, ArrayList<ProductCategory> groups, Bundle bundle) {
        mContext = context;
        mGroups = groups;
        b=bundle;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.callback = (InfoTaskListener)context;
    }

    static class OrderRowViewHolder {
        TextView name;
        TextView unitPrice;
        EditText quantity;
        TextView total;
        TextView stockStatus;
        int childPos,grpPos;
        ImageView info;
    }

    @Override
    public int getGroupCount() {
        return mGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mGroups.get(groupPosition).productItems.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        stockqnty.putDouble((mGroups.get(groupPosition).productItems.get(childPosition)).getName(),mGroups.get(groupPosition).productItems.get(childPosition).getStockQuantity());
        return mGroups.get(groupPosition).productItems.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.category_group, null);
        }

        ProductCategory group = (ProductCategory) getGroup(groupPosition);

        TextView textView = (TextView) convertView.findViewById(R.id.textView1);
        textView.setText(group.categoryName);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(R.layout.new_order_row,null);
            viewHolder = new OrderRowViewHolder();
            viewHolder.name = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_name);
            viewHolder.unitPrice = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_unit_rate);
            viewHolder.quantity = (EditText) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_quantity);
            viewHolder.total = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_bill);
            viewHolder.stockStatus = (TextView) row.findViewById(R.id.stockstatus);
            viewHolder.info = (ImageView)row.findViewById(R.id.info);
            row.setTag(viewHolder);
        } else {
            viewHolder = (OrderRowViewHolder)row.getTag();
        }
        final OrderRow item = (OrderRow)getChild(groupPosition, childPosition);

        if((item.getImageUrl().equals("null") || item.getImageUrl().equals(""))&&(item.getAudioUrl().equals("null") || item.getAudioUrl().equals("")))
        {
            viewHolder.info.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.info.setVisibility(View.VISIBLE);
        }


        viewHolder.name.setText(item.getName());
        viewHolder.unitPrice.setText('\u20B9'+item.getUnitPrice().toString());

        if(item.getStockEnabledStatus().toString().equals("true")) {
            viewHolder.stockStatus.setVisibility(View.VISIBLE);
        if(item.getStockQuantity()==0.0)
        {
          viewHolder.stockStatus.setText("OUT OF STOCK");
            viewHolder.stockStatus.setTextColor(Color.RED);
            viewHolder.quantity.setEnabled(false);
        }
        else
        {
            viewHolder.stockStatus.setText("STOCK AVAILABLE");
            viewHolder.quantity.setEnabled(true);
            viewHolder.stockStatus.setTextColor(Color.parseColor("#42A462"));
        }
        }
        else
        {

            viewHolder.stockStatus.setVisibility(View.GONE);
            viewHolder.quantity.setEnabled(true);
        }
        viewHolder.childPos=childPosition;
        viewHolder.grpPos=groupPosition;
        double quantity;

        if(b.containsKey(item.getName())){
            quantity = b.getDouble(item.getName(),0.0);
        }
        else{
            quantity = 0.0;

        }

        viewHolder.quantity.setText(quantity + "");
        item.setTotal(Double.parseDouble(String.format("%.2f", quantity * item.getUnitPrice())));
        viewHolder.total.setText(item.getTotal().toString());
        final EditText quantity_field = (EditText) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_quantity);
        final TextView total_field = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_bill);
        final TextView rate_field = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_unit_rate);
        final TextView item_name = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_name);
        final String stockEnbldStatus = item.getStockEnabledStatus();
        final String unitRate = rate_field.getText().toString().substring(1);

        quantity_field.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {


                if (!quantity_field.getText().toString().equals("")) {

                    try {
                        Double.parseDouble(quantity_field.getText().toString());
                         String str = String.format("%.2f", (Double.parseDouble(unitRate) * Double.parseDouble(quantity_field.getText().toString())));
                        total_field.setText(str + " INR");
                        b.putDouble(item_name.getText().toString(), Double.parseDouble(quantity_field.getText().toString()));

                    } catch (NumberFormatException e) {
                        Toast.makeText(mContext, R.string.label_toast_Please_enter_valid_quantity, Toast.LENGTH_SHORT).show();
                    }
                } else {
                      total_field.setText((Double.parseDouble(unitRate) * 0.0) + " INR");
                    b.putDouble(item_name.getText().toString(), 0.0);

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }
        });
        quantity_field.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasFocus) {

                if (hasFocus) {
                    if (stockEnbldStatus.equals("true")) {
                        try {
                            Double.parseDouble(quantity_field.getText().toString());
                            if (Double.parseDouble(quantity_field.getText().toString()) > stockqnty.getDouble(item_name.getText().toString()))

                                Toast.makeText(mContext, R.string.label_insufficeint_stock, Toast.LENGTH_SHORT).show();
                        } catch (NumberFormatException e) {
                        }

                    }
                }

            }
        });


        viewHolder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callback.onInfo(item.getImageUrl(),item.getAudioUrl(),item.getName());

            }

        });
        return row;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
package com.mobile.ict.cart;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;

import android.provider.MediaStore;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;


@SuppressLint("NewApi")
public class NewOrderActivity extends Activity implements OnClickListener,InfoTaskListener,SeekBar.OnSeekBarChangeListener {

    Button b1,b2,playbtn,pausebtn;
    EditText quantity;
    ProgressDialog pd;
    TextView heading,audioCurrentTimeDuration,audioTotalTimeDuration,itemName,audio;
    double grandTotal;
    Bundle bundle;
    Map <String,String> itemsMap = new HashMap<String, String>();
    Map <String,String> stockQuantityMap = new HashMap<String, String>();
    Handler handler ;
    ListView list;
    OrderRowAdapter adapter;
    ArrayList<ItemRow> itemss;
    Dialog dialog ;
    ArrayList<OrderRow> items;
    String memberNumber,stockEnabledStatus="false";
    SharedPreferences sharedPref;
    ProductCategory productCategory;
    ArrayList<ProductCategory> groups ;
    OrderRow orderRow;
    ExpandableListView listView;
    ImageView info;
    DialogPlus dialogplus;
    SeekBar audioProgressBar;
    MediaPlayer mediaPlayer;
    File imgTmpFile,audioTmpFile;
    RelativeLayout audioLayout;
    InputStream imgStream=null,audioStream=null;
    String imgUrl,audioUrl;
    URL img_url,audio_url;
    HttpURLConnection imgUrlConnection, audioUrlConnection;
    FileOutputStream imgOutStream,audioOutStream;
    Bitmap b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String fontPath1 = "fonts/Caviar_Dreams_Bold.ttf";
        Typeface tf1= Typeface.createFromAsset(getAssets(), fontPath1);

        heading=(TextView)findViewById(R.id.label_choose_order);
        b1=(Button)findViewById(com.mobile.ict.cart.R.id.button_proceed_confirm);
        b2=(Button) findViewById(com.mobile.ict.cart.R.id.button_draft);
        new LoginVerification(getApplicationContext(), itemsMap).execute(sharedPref.getString("orgAbbr",""));

        heading.setTypeface(tf1);
        b1.setTypeface(tf1);
        b2.setTypeface(tf1);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        bundle = new Bundle();
        listView = (ExpandableListView) findViewById(R.id.new_order_item_list);

        groups = new ArrayList<>();

        adapter = new OrderRowAdapter(this, groups,bundle);
        listView.setAdapter(adapter);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String imgSett = prefs.getString("groupId", "");


        handler = new Handler(){

            @Override
            public void handleMessage(Message msg) {
                // TODO Auto-generated method stub
                super.handleMessage(msg);
                pd.dismiss();
                if(getIntent().getSerializableExtra("itemsMap") != null){
                    itemsMap = (Map) getIntent().getSerializableExtra("itemsMap");
                    bundle = getIntent().getExtras();
                }
                adapter = new OrderRowAdapter(NewOrderActivity.this, groups,bundle);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

        };

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order, menu);
        MenuItem item_help = menu.findItem(R.id.action_help);
        MenuItem item_logout = menu.findItem(R.id.action_logout);
        MenuItem item_home = menu.findItem(R.id.action_home);

        MenuItem item_refresh = menu.findItem(R.id.action_refresh);

        MenuItem item_editNumber = menu.findItem(R.id.action_edit_number);

        item_editNumber.setVisible(false);
        item_editNumber.setEnabled(false);

        item_refresh.setVisible(false);
        item_refresh.setEnabled(false);

        item_home.setVisible(false);
        item_home.setEnabled(false);

        item_help.setVisible(false);
        item_logout.setVisible(false);
        item_help.setEnabled(false);
        item_logout.setEnabled(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case com.mobile.ict.cart.R.id.action_home:
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    public void onClick(View view) {
        boolean flag= true;
        itemss = new ArrayList<ItemRow>();
        Map<String, String> sortList = new TreeMap<String, String>(itemsMap);
        grandTotal=0.0;

        if(stockEnabledStatus.equals("true"))
        {
            for(Map.Entry<String, String> entry : sortList.entrySet()){
                double quantity = bundle.getDouble(entry.getKey(),0);
                if(quantity > 0) {

                    Map<String, String> stockQuantityList = new TreeMap<String, String>(stockQuantityMap);
                    for(Map.Entry<String, String> stockEntry : stockQuantityList.entrySet())
                    {
                        if(stockEntry.getKey().equals(entry.getKey()))
                        {
                            double stockQuantity = Double.parseDouble(stockEntry.getValue());
                            if(quantity>stockQuantity)
                            {
                                Toast.makeText(getApplicationContext(),R.string.label_toast_sorry_we_cannot_process_your_order_due_to_insufficent_stock,Toast.LENGTH_LONG).show();
                                itemss.clear();;
                                grandTotal=0.0;
                                flag = false;
                                break;

                            }
                        }


                    }
                    if(flag) {
                        double total = quantity * Double.parseDouble(entry.getValue());
                        grandTotal += total;
                        ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), total);
                        itemss.add(item);
                    }else{
                        break;
                    }

                }
            }
        }

        else
        {

            for(Map.Entry<String, String> entry : sortList.entrySet()){
                double quantity = bundle.getDouble(entry.getKey(),0);
                if(quantity > 0) {

                    double total = quantity * Double.parseDouble(entry.getValue());
                    grandTotal += total;
                    ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), total);
                    itemss.add(item);
                }
            }



        }


        if(view == b1){


            if(itemss.isEmpty()&&flag)
            {

                Toast.makeText(getApplicationContext(),R.string.label_toast_Please_select_at_least_one_product,Toast.LENGTH_LONG).show();

            }
            else if(!itemss.isEmpty()&&flag)
            {
                Intent i = new Intent(this,ConfirmOrderActivity.class);
                bundle.putSerializable("itemsMap", (Serializable) itemsMap);
                i.putExtras(bundle);
                startActivity(i);
                finishNewOrderActivity();

            }

        }





        if(view == b2){

            if(itemss.isEmpty()&&flag)
            {

                Toast.makeText(getApplicationContext(),R.string.label_toast_Please_select_at_least_one_product,Toast.LENGTH_LONG).show();

            }

            else if(!itemss.isEmpty()&&flag)
            {
                Intent i = new Intent(this, OrderDraftActivity.class);
                bundle.putSerializable("itemsMap", (Serializable) itemsMap);
                i.putExtras(bundle);
                startActivity(i);

            }



        }
    }

    @Override
    public void onInfo(String imageUrl, final String audioUrl,String productName)
    {

        if(connect())
        {
            utils = new Utilities();
            dialogplus = DialogPlus.newDialog(NewOrderActivity.this)
                    .setContentHolder(new ViewHolder(R.layout.info))
                    .setGravity(Gravity.CENTER)
                    .setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        }
                    })
                    .setExpanded(false)
                    .setCancelable(false)
                    .create();
            dialogplus.show();
            info = (ImageView)dialogplus.findViewById(R.id.productImg);
            itemName = (TextView)dialogplus.findViewById(R.id.productName);
            audio = (TextView)dialogplus.findViewById(R.id.audio);

            audioLayout=(RelativeLayout)dialogplus.findViewById(R.id.audioLayout);
            audioProgressBar=(SeekBar)dialogplus.findViewById(R.id.seek_bar);
            audioProgressBar.setEnabled(false);
            audioProgressBar.setOnSeekBarChangeListener(this);
            playbtn= (Button) dialogplus.findViewById(R.id.play_button);
            pausebtn= (Button) dialogplus.findViewById(R.id.pause_button);
            audioCurrentTimeDuration= (TextView) dialogplus.findViewById(R.id.songCurrentDurationLabel);
            audioTotalTimeDuration= (TextView) dialogplus.findViewById(R.id.songTotalDurationLabel);

            audioCurrentTimeDuration.setVisibility(View.INVISIBLE);
            audioTotalTimeDuration.setVisibility(View.INVISIBLE);
            Button close = (Button) dialogplus.findViewById(R.id.close);
            close.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(mediaPlayer!=null)
                    {
                        if(mediaPlayer.isPlaying())
                        {
                            mediaPlayer.stop();
                        }
                    }

                    clearBitmap();
                    dialogplus.dismiss();
                }
            });


            playbtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {



                    try {
                        mediaPlayer = new MediaPlayer();

                        mediaPlayer.reset();
                        Uri uri = Uri.parse(audioUrl);
                        mediaPlayer.setDataSource(NewOrderActivity.this, uri);
                        mediaPlayer.prepare();
                        mediaPlayer.start();
                        audioProgressBar.setProgress(0);
                        audioProgressBar.setMax(100);
                        updateProgressBar();
                    } catch (IllegalStateException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    playbtn.setEnabled(false);
                    pausebtn.setEnabled(true);
                    audioProgressBar.setEnabled(true);
                    audioCurrentTimeDuration.setVisibility(View.VISIBLE);
                    audioTotalTimeDuration.setVisibility(View.VISIBLE);

                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            playbtn.setEnabled(true);
                            pausebtn.setEnabled(false);
                            audioProgressBar.setEnabled(false);
                            audioCurrentTimeDuration.setVisibility(View.INVISIBLE);
                            audioTotalTimeDuration.setVisibility(View.INVISIBLE);
                        }
                    });

                }
            });


            pausebtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(mediaPlayer.isPlaying()){
                        mediaPlayer.pause();
                        audioProgressBar.setProgress(0);
                        pausebtn.setEnabled(false);
                        playbtn.setEnabled(true);

                    }
                    else{
                        pausebtn.setEnabled(false);
                        playbtn.setEnabled(true);

                    }
                    audioProgressBar.setEnabled(false);
                    audioProgressBar.setProgress(0);
                    audioCurrentTimeDuration.setVisibility(View.INVISIBLE);
                    audioTotalTimeDuration.setVisibility(View.INVISIBLE);
                    mHandler.removeCallbacks(mUpdateTimeTask);

                }
            });




            if((imageUrl.equals("null") || imageUrl.equals("")))
            {
                info.setVisibility(View.GONE);
                itemName.setVisibility(View.GONE);
                Toast.makeText(this,R.string.label_toast_No_image_available_for_this_product,Toast.LENGTH_SHORT).show();

            }
            else if((audioUrl.equals("null") || audioUrl.equals("")))
            {
                itemName.setText(productName);
                audio.setVisibility(View.GONE);
                audioLayout.setVisibility(View.GONE);
                Toast.makeText(this,R.string.label_toast_No_audio_available_for_this_product,Toast.LENGTH_SHORT).show();
                b=null;
                new InfoLoaderTask().execute(imageUrl);
            }
            else
            {
                itemName.setText(productName);
                b=null;
                new InfoLoaderTask().execute(imageUrl);
            }

        }
        else
        {
            Toast.makeText(getApplicationContext(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();


        }


    }


    Handler mHandler = new Handler();
    Utilities utils;

    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = mediaPlayer.getDuration();
            long currentDuration = mediaPlayer.getCurrentPosition();


            audioTotalTimeDuration.setText("" + utils.milliSecondsToTimer(totalDuration));

            audioCurrentTimeDuration.setText("" + utils.milliSecondsToTimer(currentDuration));


            int progress = (int)(utils.getProgressPercentage(currentDuration, totalDuration));

            audioProgressBar.setProgress(progress);
            if(!(mediaPlayer.isPlaying())){
                mHandler.removeCallbacks(mUpdateTimeTask);
                return ;
            }

            mHandler.postDelayed(this, 100);
        }
    };


    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if(b)
            mediaPlayer.seekTo(i);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = mediaPlayer.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

        mediaPlayer.seekTo(currentPosition);


        updateProgressBar();
    }


    private class InfoLoaderTask extends AsyncTask<String ,Void, Bitmap>{

        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(NewOrderActivity.this);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected Bitmap doInBackground(String... url) {


            try {

                    img_url = new URL(url[0]);

                    imgUrlConnection = (HttpURLConnection) img_url.openConnection();

                    imgUrlConnection.connect();

                    imgStream = imgUrlConnection.getInputStream();

                    b = BitmapFactory.decodeStream(imgStream);

                    imgStream.close();
                    imgStream=null;

                    return  b;


            }
            catch (RuntimeException re)
            {
            }
            catch (Exception e) {

                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {

            pd.dismiss();


            if(result!=null)
            {
                result = readBitmap(getImageUri(NewOrderActivity.this, result), NewOrderActivity.this, 2);
                info.setImageBitmap(result);


            }



        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public  Bitmap readBitmap(Uri selectedImage, Context context, int scalValue) {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = scalValue;
        AssetFileDescriptor fileDescriptor =null;
        try {
            fileDescriptor = context.getContentResolver().openAssetFileDescriptor(selectedImage, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finally{
            try {
                bm = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);
                fileDescriptor.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bm;
    }

    public void clearBitmap() {

       if(b!=null)
       {
           b.recycle();
           System.gc();
       }



    }

     @SuppressLint("NewApi")
    public class LoginVerification extends AsyncTask<String, String, String> {


        public LoginVerification(Context context, Map<String, String> map) {
            itemsMap = map;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(NewOrderActivity.this);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String url = "http://ruralict.cse.iitb.ac.in/RuralIvrs/api/products/search/byType/map?orgabbr=" + params[0];

            // String url = "http://ruralict.cse.iitb.ac.in/ruralict/api/products/search/byType/map?orgabbr=" + params[0];

            JsonParser jParser = new JsonParser();

            String response = jParser.getJSONFromUrl(url, null, "GET", true, sharedPref.getString("emailid", null), sharedPref.getString("password", null));


            return response;

        }

        @Override
        protected void onPostExecute(String response)
        {
            pd.dismiss();

            try {
                JSONObject object = new JSONObject(response);

                itemsMap = JsonParser.categoryMap(object);

                stockQuantityMap = JsonParser.stockQuantityMap(object);

                groups = new ArrayList<>();

                Iterator<String> keysItr = object.keys();
                while(keysItr.hasNext())
                {
                    String key = keysItr.next();

                    productCategory= new ProductCategory(key);
                    Object category =object.get(key);
                    if(category instanceof JSONArray)
                    {
                        for(int i=0; i<((JSONArray)category).length();i++)
                        {

                            if(((JSONArray)category).getJSONObject(i).getString("imageUrl") == null && ((JSONArray)category).getJSONObject(i).getString("audioUrl") == null)
                            {
                                orderRow=new OrderRow(((JSONArray)category).getJSONObject(i).getString("name"),Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),0.0,Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("quantity")),((JSONArray)category).getJSONObject(i).getString("stockManagement"), "null", "null");
                            }
                            else if(((JSONArray)category).getJSONObject(i).getString("imageUrl") == null)
                            {
                                orderRow=new OrderRow(((JSONArray)category).getJSONObject(i).getString("name"),Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),0.0,Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("quantity")),((JSONArray)category).getJSONObject(i).getString("stockManagement"), "null", ((JSONArray)category).getJSONObject(i).getString("audioUrl"));
                            }
                            else if (((JSONArray)category).getJSONObject(i).getString("audioUrl") == null)
                            {
                                orderRow=new OrderRow(((JSONArray)category).getJSONObject(i).getString("name"),Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),0.0,Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("quantity")),((JSONArray)category).getJSONObject(i).getString("stockManagement"), ((JSONArray)category).getJSONObject(i).getString("imageUrl"), "null");
                            }
                            else
                            {
                                orderRow=new OrderRow(((JSONArray)category).getJSONObject(i).getString("name"),Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),0.0,Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("quantity")),((JSONArray)category).getJSONObject(i).getString("stockManagement"), ((JSONArray)category).getJSONObject(i).getString("imageUrl"), ((JSONArray)category).getJSONObject(i).getString("audioUrl"));
                            }

                            stockEnabledStatus = ((JSONArray)category).getJSONObject(i).getString("stockManagement");

                            productCategory.productItems.add(orderRow);
                        }

                    }

                    groups.add(productCategory);

                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block


                AlertDialog.Builder builder = new AlertDialog.Builder(NewOrderActivity.this);

                builder.setTitle(R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later)
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                finish();

                            }
                        })

                        .setCancelable(false);

                dialog=builder.create();
                dialog.show();

            }

            Message msg = Message.obtain();
            handler.sendMessage(msg);


        }

    }

    public boolean connect(){
        String connection= "false";
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){

            connection="true";
        }
        Boolean con = Boolean.valueOf(connection);
        return con;

    }


    public void finishNewOrderActivity() {
        NewOrderActivity.this.finish();
    }

}
